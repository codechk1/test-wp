<?php get_header();

$queried_object = get_queried_object(); ?>

<div class="container">
  <div class="row">
          <div class="col-lg-8">



<?php
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $args = array('post_type' => 'story', 'posts_per_page' => 2, 'paged' => $paged);
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
?>

<div <?php post_class() ?>>
            
                    <h2 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>
          <div class="single-image"><?php the_post_thumbnail(); ?></div>
          <?php //description(); ?>
         
          <div class="meta">
             <div class="time"><span><i class="far fa-calendar-alt"></i> <?php the_time('F d, Y') ?></span><span><i class="fas fa-user"></i> <?php the_field('writer_name');?></span><span><i class="fas fa-tags"></i> <?php the_field('catagory');?></span>



<script type="text/javascript"><!--
    var dropdown = document.getElementById("cat");
    function onCatChange() {
        if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
            location.href = "<?php echo home_url();?>/category/"+dropdown.options[dropdown.selectedIndex].value+"/";
        }
    }
    dropdown.onchange = onCatChange;
--></script></div>
               
                <?php //comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
              </div>
                    <div class="entry">
                        <?php the_excerpt(); ?>
                    </div>
          <div class="blog-button wow fadeInUp"><a class="read" href="<?php the_permalink(); ?>">Read More</a></div>
   </div>

<?php endwhile; wp_reset_postdata(); ?>

<div id="pagination" class="clearfix">
    
    <?php 

     
    posts_nav_link( ' &nbsp; ', '<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>' ); ?>

</div>



</div>


<div class="col-lg-4"><div id="secondary" class="secondary"><?php dynamic_sidebar( 'sidebar-2' ); ?></div></div>

</div>
</div>

















<?php get_footer(); ?>