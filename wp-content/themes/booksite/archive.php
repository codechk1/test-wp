<?php get_header(); ?>

  <?php if (have_posts()) : ?>
<div class="container">
  <div class="row">
		  <div class="col-lg-8">
 			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

			<?php /* If this is a category archive */ if (is_category()) { ?>

				<h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>

			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h2>Archive for <?php the_time('F jS, Y'); ?></h2>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h2>Archive for <?php the_time('F, Y'); ?></h2>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h2>Archive for <?php the_time('Y'); ?></h2>

			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h2>Author Archive</h2>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h2>Blog Archives</h2>
			
			<?php } ?>

			<?php //include (TEMPLATEPATH . '/inc/nav.php' ); ?>

			<?php while (have_posts()) : the_post(); ?>
			
				<div <?php post_class() ?>>
				
					<h2 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>
          <div class="single-image"><?php the_post_thumbnail(); ?></div>
          <div class="meta">
               <div class="time wow fadeInUp"><span><i class="far fa-calendar-alt"></i> <?php the_time('F d, Y') ?></span><span><i class="fas fa-user"></i> <?php the_author() ?></span><span><i class="fas fa-tags"></i> <?php the_category(', ') ?></span></div>
                <?php //comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
              </div>
					<div class="entry">
						<?php the_excerpt(); ?>
					</div>
          <div class="blog-button wow fadeInUp"><a class="read" href="<?php the_permalink(); ?>">Read More</a></div>
           <div class="postmetadata">
                <?php //the_tags('Tags: ', ', ', '<br />'); ?>
                <?php //the_category(', ') ?>  
                <?php //comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
              </div>
          </div>
			
        
          
			<?php endwhile; ?>
        
        <div class="pagination"><?php posts_nav_link( ' &nbsp; ', '<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>' ); ?></div>
    </div>      
  
<div class="col-lg-4"><?php get_sidebar(); ?></div>
   </div>

</div>
	<?php else : ?>

		<h2>Nothing found</h2>

   
       
  
	<?php endif; ?>

<?php get_footer(); ?>