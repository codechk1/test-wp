jQuery(document).ready(function() {
  
  jQuery(window).load(function() {
        // will first fade out the loading animation
    jQuery("#status").fadeOut();
        // will fade out the whole DIV that covers the website.
    jQuery("#preloader").delay(1000).fadeOut("slow");
   jQuery('body').delay(350).css({'overflow':'visible'});
})
  
jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    dots :false, 
    navText:['<i class="fas fa-arrow-left"></i>','<i class="fas fa-arrow-right"></i>'],
    navigation: true,
    items: 1
}); 



  jQuery('.humburger').click(function(){
    jQuery('#site-navigation').show();
  });

  jQuery('.closed a').click(function(){
    jQuery('#site-navigation').hide();
  });
  
jQuery('.story .outer:nth-of-type(odd)').addClass('fadeInLeft slideInLeft');
jQuery('.story .outer:nth-of-type(even)').addClass('fadeInRight slideInRight');

 

new WOW().init();

  
  });




