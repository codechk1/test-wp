<?php
// Essentials
include_once 'libs/config.php';
//require_once( THEME_FRAMEWORK_DIR . '/widgets/recentpost.php' );


// Get Post Featured Image URL
function smart_featured_img_url($smart_featured_img_size) {
$smart_image_id = get_post_thumbnail_id();
$smart_image_url = wp_get_attachment_image_src($smart_image_id,$smart_featured_img_size);
$smart_image_url = $smart_image_url[0];
return $smart_image_url;
}

// Get Post Featured Pluse Post Editor Image
function smart_get_first_image($image_size){
global $post, $posts;
$first_img = '';
ob_start();
ob_end_clean();
$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

$first_img="";

if(smart_featured_img_url($image_size))
$first_img = smart_featured_img_url($image_size);
else
$first_img = $matches[1][0];


return $first_img;
}


function SearchFilter($query) {
    // If 's' request variable is set but empty
    if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
        $query->is_search = true;
        $query->is_home = false;
    }
    return $query;}
add_filter('pre_get_posts','SearchFilter');



function add_custom_image_Files($post_id, $upload,$is_featured=false)
{
 
 if (!function_exists('wp_generate_attachment_metadata')){
  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');
 }
 $uploads = wp_upload_dir(); /*Get path of upload dir of wordpress*/

 if (is_writable($uploads['path']))  /*Check if upload dir is writable*/
 {
  if ((!empty($upload['tmp_name'])))  /*Check if uploaded image is not empty*/
  {
   if ($upload['tmp_name'])   /*Check if image has been uploaded in temp directory*/
   {
    $file=handle_image_upload($upload); /*Call our custom function to ACTUALLY upload the image*/

    $attachment = array  /*Create attachment for our post*/
    (
      'post_mime_type' => $file['type'],  /*Type of attachment*/
   'post_title' => preg_replace('/\.[^.]+$/', '', basename($file['file'])),
 'post_name' => preg_replace('/\.[^.]+$/', '', basename($file['file'])),
      'post_parent' => $post_id,  /*Post id*/
    );

    $aid = wp_insert_attachment($attachment, $file['file'], $post_id);  /*Insert post attachment and return the attachment id*/
    if($is_featured){set_post_thumbnail( $post_id , $aid);}
    $a = wp_generate_attachment_metadata($aid, $file['file'] );  /*Generate metadata for new attacment*/
   
    update_post_meta($post_id, 'custom_image', $aid);  /*Save the attachment id in meta data*/

    if ( !is_wp_error($aid) ) 
    {
     wp_update_attachment_metadata($aid, wp_generate_attachment_metadata($aid, $file['file'] ) );  /*If there is no error, update the metadata of the newly uploaded image*/
    }
    return $aid;
   }
  }
  else
  {
   echo 'Please upload the image.';
  }
 }
}

function handle_image_upload($upload)
{
 if (!function_exists('wp_generate_attachment_metadata')){
  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');
 }
 global $post;

        if (file_is_displayable_image( $upload['tmp_name'] )) /*Check if image*/
        {
            /*handle the uploaded file*/
            $overrides = array('test_form' => false);
            $file=wp_handle_upload($upload, $overrides);
        }

 return $file;

}





// First we create a function
function list_terms_custom_taxonomy( $atts ) {
 
// Inside the function we extract custom taxonomy parameter of our shortcode
 
    extract( shortcode_atts( array(
        'custom_taxonomy' => 'cat_story',
    ), $atts ) );
 
// arguments for function wp_list_categories
$args = array( 
taxonomy => $custom_taxonomy,
title_li => ''
);
 
// We wrap it in unordered list
echo '<section><ul>'; 
echo wp_list_categories($args);
echo '</ul></section>';
}
 
// Add a shortcode that executes our function
add_shortcode( 'ct_terms', 'list_terms_custom_taxonomy' );
 
//Allow Text widgets to execute shortcodes
 
add_filter('widget_text', 'do_shortcode');

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );




add_action( 'pre_get_posts', function ( $q ) {

    if( !is_admin() && $q->is_main_query() && $q->is_post_type_archive( 'story' ) ) {

        $q->set( 'posts_per_page', 2 );

    }

});



