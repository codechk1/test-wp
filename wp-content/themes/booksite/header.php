<?php
ob_start();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div id="preloader">
   <div id="status">
     <i class="fas fa-book-open wow pulse animated" data-wow-iteration="infinite" data-wow-duration="2s"></i>
      <i class="fas fa-book-open wow pulse animated" data-wow-iteration="infinite" data-wow-duration="2s"></i>
      <i class="fas fa-book-open wow pulse animated" data-wow-iteration="infinite" data-wow-duration="2s"></i>
    </div>
</div>
   <!--<div class="hameid-loader-overlay"></div>-->
 <header>
  <!-- <div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
       <div class="loader-section section-right"></div>
	</div>-->
   
<div class="container align-items-center d-flex justify-content-between">
  <div id="logo">
  	<a href="<?php echo get_option("siteurl"); ?>">
  		Book Site
  	</a>
  </div>
  
  <a class="humburger" href="javascript:void(0)">
  	<span></span>
  	<span></span>
  	<span></span>
  </a>

<?php if ( has_nav_menu( 'primary' ) ) : ?>
	<nav id="site-navigation" class="main-navigation" role="navigation">
		<?php
			// Primary navigation menu.
			wp_nav_menu( array(
				'menu_class'     => 'nav-menu',
				'theme_location' => 'primary',
			) );
		?>
	</nav>
	<?php endif; ob_flush(); ?>
</div> 

</header>
 

<!-- .main-navigation -->
