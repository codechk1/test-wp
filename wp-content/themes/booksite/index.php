<?php ob_start(); ?>
<?php get_header(); ?>
 <div class="container">
    <div class="row">
      <div class="col-lg-8">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   
     
       
		     <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
           <h2 class="wow fadeInUp" data-wow-duration="1.5s"><?php the_title(); ?></h2>
              <div class="single-image wow fadeInUp" data-wow-duration="1.5s"><?php the_post_thumbnail(); ?></div>
             
       
              <div class="meta wow fadeInUp" data-wow-duration="1.5s">
                 <div class="time wow fadeInUp"><span><i class="far fa-calendar-alt"></i> <?php the_time('F d, Y') ?></span><span><i class="fas fa-user"></i> <?php the_author() ?></span><span><i class="fas fa-tags"></i> <?php the_category(', ') ?></span></div>
               
                <?php //comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
              </div>
              <div class="entry wow fadeInUp" data-wow-duration="1.5s">
                <?php the_excerpt(); ?>
              </div>
            <div class="blog-button wow fadeInUp" data-wow-duration="1.5s"><a class="read" href="<?php the_permalink(); ?>">Read More</a></div>
              <div class="postmetadata wow fadeInUp">
                <?php //the_tags('Tags: ', ', ', '<br />'); ?>
                <?php //the_category(', ') ?>  
                <?php //comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
              </div>
         
      
          </div>
    
       
      

	<?php endwhile; ?>

	<?php //include (TEMPLATEPATH . '/inc/nav.php' ); ?>
        <?php posts_nav_link( ' &#183; ', '<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>' ); ?>
        </div>
       <div class="col-lg-4"><?php get_sidebar(); ?></div>
</div>
   </div>
	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>



<?php get_footer(); ?>
<?php ob_flush(); ?>