<?php
// Get Theme Data from style.css
$theme_data = wp_get_theme();

// Core
define( 'THEME_NAME', $theme_data->Name );
define( 'THEME_VERSION', $theme_data->Version );

define( 'THEME_URI', get_template_directory_uri() );
define( 'THEME_DIR', get_template_directory() );

define( 'THEME_FRAMEWORK_URI', THEME_URI.'/libs' );
define( 'THEME_FRAMEWORK_DIR', THEME_DIR.'/libs' );

define( 'THEME_CUSTOM_ASSETS_IMAGES', THEME_URI.'/assets/images' );
define( 'THEME_CUSTOM_ASSETS_SCTIPT', THEME_URI.'/assets/js' );
define( 'THEME_CUSTOM_ASSETS_STYLE', THEME_URI.'/assets/css' );


if ( ! function_exists( 'booksite_setup' ) ) :
function booksite_setup() {
	
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 0, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'booksite' ),
		'footer'  => __( 'Footer Links Menu', 'booksite' ),
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	
}
endif; // booksite_setup
add_action( 'after_setup_theme', 'booksite_setup' );


/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function booksite_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'booksite' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'booksite' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

  register_sidebar( array(
		'name'          => __( 'Footer', 'booksite' ),
		'id'            => 'footer',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'booksite' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s col-md-12">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
  
   register_sidebar( array(
		'name'          => __( 'Story', 'booksite' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'booksite' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s secondary">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
  
}
add_action( 'widgets_init', 'booksite_widgets_init' );


// Register Custom Post Type

function register_story_posttype() { 



	$labels = array(

		'name'                => _x( 'Stories', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Story', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Story', 'text_domain' ),

		'parent_item_colon'   => __( 'Parent Story:', 'text_domain' ),

		'all_items'           => __( 'All Stories', 'text_domain' ),

		'view_item'           => __( 'View Story', 'text_domain' ),

		'add_new_item'        => __( 'Add New Story', 'text_domain' ),

		'add_new'             => __( 'New Story', 'text_domain' ),

		'edit_item'           => __( 'Edit Story', 'text_domain' ),

		'update_item'         => __( 'Update Story', 'text_domain' ),

		'search_items'        => __( 'Search Stories', 'text_domain' ),

		'not_found'           => __( 'No Stories found', 'text_domain' ),

		'not_found_in_trash'  => __( 'No Stories found in Trash', 'text_domain' ),

	);

	$args = array(

		'label'               => __( 'Story', 'text_domain' ),

		'description'         => __( 'Story information pages', 'text_domain' ),

		'labels'              => $labels,

		//'taxonomies'        => array( 'category', 'post_tag' ),

		'hierarchical'        => false,

		'public'              => true,

		'show_ui'             => true,

		'show_in_menu'        => true,

		'show_in_nav_menus'   => true,

		'show_in_admin_bar'   => true,

		'menu_position'       => 5,

		//'menu_icon'         => '',

		'can_export'          => true,

		'has_archive'         => true,

		'exclude_from_search' => false,

		'publicly_queryable'  => true,

		'capability_type'     => 'post',
		
		'supports'            => array( 'title', 'editor', 'thumbnail', 'post-formats' ),

	);
	register_post_type( 'story', $args );

}


// Hook into the 'init' action

add_action( 'init', 'register_story_posttype', 0 ); 




// Register Custom Taxonomy

function custom_taxonomy() {



	$labels = array(

		'name'                       => _x( 'Stories', 'Taxonomy General Name', 'text_domain' ),

		'singular_name'              => _x( 'Story', 'Taxonomy Singular Name', 'text_domain' ),

		'menu_name'                  => __( 'Category', 'text_domain' ),

		'all_items'                  => __( 'All Items', 'text_domain' ),

		'parent_item'                => __( 'Parent Item', 'text_domain' ),

		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),

		'new_item_name'              => __( 'New Category Name', 'text_domain' ),

		'add_new_item'               => __( 'Add New Category', 'text_domain' ),

		'edit_item'                  => __( 'Edit Category', 'text_domain' ),

		'update_item'                => __( 'Update Category', 'text_domain' ),

		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),

		'search_items'               => __( 'Search Items', 'text_domain' ),

		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),

		'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),

		'not_found'                  => __( 'Not Found', 'text_domain' ),



	);

	$args = array(

		'labels'                     => $labels,

		'hierarchical'               => true,

		'public'                     => true,

		'show_ui'                    => true,

		'show_admin_column'          => true,

		'show_in_nav_menus'          => true,

		'show_tagcloud'              => true,

		'save_action'   => 'save_taxonomy_terms',

    //'rewrite'           => array( 'slug' => 'story' ), // change this slug

	);

	register_taxonomy( 'cat_story', array( 'story' ), $args );

}


add_action( 'init', 'custom_taxonomy', 0 );


/**/
// Register Custom Post Type

function register_slider_posttype() { 



	$labels = array(

		'name'                => _x( 'Home Slider', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Home Slider', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Home Slider', 'text_domain' ),

	//	'parent_item_colon'   => __( 'Parent Story:', 'text_domain' ),

		'all_items'           => __( 'All Home Slider', 'text_domain' ),

		'view_item'           => __( 'View Home Slider', 'text_domain' ),

		'add_new_item'        => __( 'Add New Home Slider', 'text_domain' ),

		'add_new'             => __( 'New Home Slider', 'text_domain' ),

		'edit_item'           => __( 'Edit Home Slider', 'text_domain' ),

		'update_item'         => __( 'Update Home Slider', 'text_domain' ),

		'search_items'        => __( 'Search Home Sliders', 'text_domain' ),

		'not_found'           => __( 'No Home Sliders found', 'text_domain' ),

		'not_found_in_trash'  => __( 'No Home Sliders found in Trash', 'text_domain' ),

	);

	$args = array(

		'label'               => __( 'Home Slider', 'text_domain' ),

		'description'         => __( 'Home Slider information pages', 'text_domain' ),

		'labels'              => $labels,

		'supports'            => array( 'title', 'editor', 'thumbnail', 'post-formats' ),

		//'taxonomies'        => array( 'category', 'post_tag' ),

		'hierarchical'        => false,

		'public'              => true,

		'show_ui'             => true,

		'show_in_menu'        => true,

		'show_in_nav_menus'   => true,

		'show_in_admin_bar'   => true,

		'menu_position'       => 5,

		//'menu_icon'         => '',

		'can_export'          => true,

		'has_archive'         => true,

		'exclude_from_search' => false,

		'publicly_queryable'  => true,

		'capability_type'     => 'post',

	);
	register_post_type( 'home_slider', $args );

}


// Hook into the 'init' action

add_action( 'init', 'register_slider_posttype', 0 ); 




/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function booksite_scripts() {
	
	// Theme stylesheet.
   wp_enqueue_style( 'booksite-font', 'https://fonts.googleapis.com/css?family=Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', true, THEME_VERSION);
   
   wp_enqueue_style( 'booksite-Libre', 'https://fonts.googleapis.com/css?family=Libre+Baskerville', true, THEME_VERSION); 
  
 // wp_enqueue_style( 'booksite-fontawesome',  THEME_CUSTOM_ASSETS_STYLE. '/font-awesome.min.css', '20150825', true );
 
  wp_enqueue_style( 'booksite-owl',  THEME_CUSTOM_ASSETS_STYLE. '/owl.carousel.min.css', '20150825', true );

  wp_enqueue_style( 'booksite-all',  THEME_CUSTOM_ASSETS_STYLE. '/all.css', '20150825', true );

 // wp_enqueue_style( 'booksite-animate',  THEME_CUSTOM_ASSETS_STYLE. '/animate.css', '20150825', true );

  wp_enqueue_style( 'booksite-bootstrapstyle',  THEME_CUSTOM_ASSETS_STYLE. '/bootstrap.min.css', '20150825', true );

  wp_enqueue_style( 'booksite-style', get_stylesheet_uri() );
  

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	

//	wp_enqueue_script( 'booksite-plugins',  THEME_CUSTOM_ASSETS_SCTIPT. '/plugins.js', array( 'jquery' ), '20150825', true );

//  wp_enqueue_script( 'booksite-jquerymin',  THEME_CUSTOM_ASSETS_SCTIPT. '/jquery-3.3.1.min.js', array( 'jquery' ), '20150825', true );

  wp_enqueue_script( 'booksite-wow',  THEME_CUSTOM_ASSETS_SCTIPT. '/wow.min.js', array( 'jquery' ), '20150825', true );

  wp_enqueue_script( 'booksite-fontawesome',  THEME_CUSTOM_ASSETS_SCTIPT. '/all.js', array( 'jquery' ), '20150825', true );

	wp_enqueue_script( 'booksite-bootstrap4',  THEME_CUSTOM_ASSETS_SCTIPT. '/bootstrap.min.js', array( 'jquery' ), '20150825', true );

  wp_enqueue_script( 'booksite-owlcarousel',  THEME_CUSTOM_ASSETS_SCTIPT. '/owl.carousel.min.js', array( 'jquery' ), '20150825', true );

  wp_enqueue_script( 'booksite-common',  THEME_CUSTOM_ASSETS_SCTIPT. '/common.js', array( 'jquery' ), '20150825', true );
  
	
}
add_action( 'wp_enqueue_scripts', 'booksite_scripts' );