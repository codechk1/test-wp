<?php
/**
 * Template Name: Front Page Template
 *
 */

get_header(); ?>
<div class="home-slider">
<div class="owl-carousel owl-theme">
      <?php 
       
$args = array( 'post_type' => 'home_slider', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
?>
<div class="item">
  <div class="banner-image wow fadeInUp">
  <?php the_post_thumbnail( 'full' ); ?>
 
<div class="banner-content">   
  <div>
  <h2 class="wow fadeInLeft home-heading" data-wow-duration="1.5s"><?php the_title();?><!--<a class="wow fadeInUp" href="<?php the_field('slider_button');?>"></a>--></h2>
  <p class="wow fadeInLeft"><?php the_content(); ?></p>
  <!--<a class="more wow fadeInUp" href="<?php //the_field('slider_button');?>">More</a>-->
  </div>
    </div>
     </div>
  </div>
  <?php
endwhile;
      ?>
    </div>
</div>

<div class="about-us">
<div class="container">
  <?php if ( have_posts() ) :
  while ( have_posts() ) : the_post();
   the_content();
  endwhile;  ?>
<?php else :
  echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div>
</div>



<div class="home-story">
  
  <?php 
   $args = array( 'post_type' => 'story', 'posts_per_page' => 6 );
  $loop = new WP_Query( $args );

  while ( $loop->have_posts() ) : $loop->the_post();
   ?>
    
   <div class="outer wow d-flex align-items-center">
     <div class="story-image wow fadeInUp"><?php the_post_thumbnail(); ?></div>
      <div class="inner wow fadeInUp">
       <h2 class="home-heading wow fadeInUp"><?php the_title(); ?></h2>
       <!--<div class="writer_pic"><img src="<?php echo the_field('writer_pic'); ?>" alt="" /></div>-->
       <div class="time wow fadeInUp"><span><i class="far fa-calendar-alt"></i> <?php the_time('F m, Y') ?></span><span><i class="fas fa-user"></i> <?php the_field('writer_name');?></span><span><i class="fas fa-tags"></i> <?php the_field('catagory');?></span></div>
       <div class="content wow fadeInUp"><?php the_excerpt();?></div>
       <div class="blog-button wow fadeInUp"><a class="read" href="<?php the_permalink(); ?>">Continue Reading</a></div>
    </div>
  </div>
  <?php
  endwhile;
  
  ?>
  </div>

<?php get_footer(); ?>