<?php/**
Template Name:News Add
 *
*/
// get_header(); ?>
 
 <?php
 $postTitleError = '';
 $postContentError = '';
 
 if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "frontnewPost") {
 
 if ( trim( $_POST['postTitle'] ) === '' ) {
 $postTitleError = 'Please enter a title.';
 $hasError = true;
 }
 if ( trim( $_POST['postContent'] ) === '' ) {
 $postContentError = 'Please enter the post content.';
 $hasError = true;
 }
 
 if (isset ($_POST['postTitle'])) {
         $postTitle =  $_POST['postTitle'];
     }
 
     if (isset ($_POST['postContent'])) {
         $postContent = $_POST['postContent'];
     } 
 
 if ( isset($_POST['postTitle']) && isset($_POST['postContent']) && ( $hasError==false )) {
 $postTags = trim($_POST['postTags']);
 global $wpdb;
 
     	$new_post = array(
     'post_title'    =>   $postTitle,
     'post_content'  =>   $postContent,
     	'post_category' =>   $_POST['cat'],//array($_POST['cat']), // if specific category, then set it's id like: array(4),  
     'tags_input'    =>   array($postTags),
     	'post_status'   =>   'publish',         
     'post_type' =>   'story'  
     );
 
     	$post_id = wp_insert_post($new_post);
  
     	wp_set_post_tags($post_id, $_POST['postTags']);
 
     	$link = get_permalink( $post_id );
 echo "<meta http-equiv='refresh' content='0;url=$link' />"; exit;
 }
 

   
 }
 

require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');
 
// example image
$image = 'http://example.com/logo.png';
 
// magic sideload image returns an HTML image, not an ID
$media = media_sideload_image($image, $post_id);
 
// therefore we must find it so we can set it as featured ID
if(!empty($media) && !is_wp_error($media)){
    $args = array(
        'post_type' => 'story',
        'posts_per_page' => -1,
        'post_status' => 'any',
        'post_parent' => $post_id
    );
 
    // reference new image to set as featured
    $attachments = get_posts($args);
 
    if(isset($attachments) && is_array($attachments)){
        foreach($attachments as $attachment){
            // grab source of full size images (so no 300x150 nonsense in path)
            $image = wp_get_attachment_image_src($attachment->ID, 'full');
            // determine if in the $media image we created, the string of the URL exists
            if(strpos($media, $image[0]) !== false){
                // if so, we found our image. set it as thumbnail
                set_post_thumbnail($post_id, $attachment->ID);
                // only want one image
                break;
            }
        }
    }
}













?>
 <div id="primary" class="site-content">
 <div id="content" role="main">
 
 <div class="form-content container">
 
     <!-- FORM -->
 <div class="wpcf7">
 <form id="new_post" name="new_post" method="post" action="" class="wpcf7-form form-horizontal" enctype="multipart/form-data" role="form">
 
 <div class="required">
 <?php if ( $postTitleError != '' ) { ?>
 <span class="error"><?php echo $postTitleError; ?></span>
 <div class="clearfix"></div>
 <?php } ?>
 <?php if ( $postContentError != '' ) { ?>
 <span class="error"><?php echo $postContentError; ?></span>
 <div class="clearfix"></div>
 <?php } ?>
 </div> 
 <br/>
 
 <!-- post name -->
 <div class="control-group string postTitle">
 <label for="postTitle" class="string control-label">Title</label>
 <div class="controls">
 <input type="text" name="postTitle" id="postTitle" class="string span6" value="<?php if ( isset( $_POST['postTitle'] ) ) echo $_POST['postTitle']; ?>">
 </div>
 </div>
 <br/>
 <!-- post Content -->
 <div class="control-group string postContent">
 <label for="postContent" class="string control-label">Contents</label>
 <div class="controls">
 <textarea id="postContent" tabindex="15" name="postContent" cols="80" rows="10"><?php if ( isset( $_POST['postContent'] ) ) { if ( function_exists( 'stripslashes' ) ) { echo stripslashes( $_POST['postContent'] );} else { echo $_POST['postContent'];}} ?></textarea>
 </div>
 </div>
 <br/>
 
 <!-- post Category  -->
 <div class="control-group string postCategory">
 <label for="postCategory" class="string control-label">Category</label>
 <div class="controls">
 <?php // wp_dropdown_categories(); ?>
 <?php $select_cats = wp_dropdown_categories( array( 'echo' => 0, 'taxonomy' => 'category', 'hide_empty' => 0 ) );
   $select_cats = str_replace( "name='cat' id=", "name='cat[]' multiple='multiple' id=", $select_cats );
   echo $select_cats; ?>
 </div>
 </div>
 <br/>
 
 <!-- post tags -->
 <div class="control-group string postTags">
 <label for="postTags" class="string control-label">Tags (comma separated)</label>
 <div class="controls">
 <input type="text" name="postTags" id="postTags" class="string span6">
  </div>
 </div>
 
 <br/>
 <fieldset class="submit">
 <input type="submit" value="Post Review" tabindex="40" id="submit" name="submit" class="btn-sm" />
 </fieldset>
 
 <input type="hidden" name="action" value="frontnewPost" />
 </form>
 </div> 
 <!-- END WPCF7 -->
 
     <!-- END OF FORM -->
     </div>
 
 
 
 
 </div><!-- #content -->
 </div><!-- #primary -->

   
   
   
   
   
   
   
   
  
<!---->

<?php 

 $postTitleError = '';
 $postContentError = '';
 
 if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "frontnewPost" ) {
 
 if ( trim( $_POST['postTitle'] ) === '' ) {
 $postTitleError = 'Please enter a title.';
 $hasError = true;
 }
 if ( trim( $_POST['postContent'] ) === '' ) {
 $postContentError = 'Please enter the post content.';
 $hasError = true;
 }
 
 if (isset ($_POST['postTitle'])) {
         $postTitle =  $_POST['postTitle'];
     }
 
     if (isset ($_POST['postContent'])) {
         $postContent = $_POST['postContent'];
     } 
 
 if ( isset($_POST['postTitle']) && isset($_POST['postContent']) && ( $hasError==false )) {
 $postTags = trim($_POST['postTags']);
 global $wpdb;
 
     	$new_post = array(
     'post_title'    =>   $postTitle,
     'post_content'  =>   $postContent,
     'post_category' =>   $_POST['cat'],//array($_POST['cat']), // if specific category, then set it's id like: array(4),  
     'tags_input'    =>   array($postTags),
     'post_status'   =>   'pending',         
     'post_type' =>   'story'  
     );
 
     	$post_id = wp_insert_post($new_post);
  
     	wp_set_post_tags($post_id, $_POST['postTags']);
 
     	$link = get_permalink( $post_id );
 echo "<meta http-equiv='refresh' content='0;url=$link' />"; exit;
 }
 
  
   
 
 }


?>


    <!-- FORM -->
 <div class="wpcf7">
 <form id="new_post" name="new_post" method="post" action="" class="wpcf7-form form-horizontal" enctype="multipart/form-data" role="form">
 
 <div class="control-group string postTitle">
 <label for="postTitle" class="string control-label">Title</label>
 <div class="controls">
 <input type="text" name="postTitle" id="postTitle" class="string span6" value="<?php if ( isset( $_POST['postTitle'] ) ) echo $_POST['postTitle']; ?>">
 </div>
 </div>
 <br/>
 <!-- post Content -->
 <div class="control-group string postContent">
 <label for="postContent" class="string control-label">Contents</label>
 <div class="controls">
 <textarea id="postContent" tabindex="15" name="postContent" cols="80" rows="10"><?php if ( isset( $_POST['postContent'] ) ) { if ( function_exists( 'stripslashes' ) ) { echo stripslashes( $_POST['postContent'] );} else { echo $_POST['postContent'];}} ?></textarea>
 </div>
 </div>
 <br/>
 
 <!-- post Category  -->
 <div class="control-group string postCategory">
 <label for="postCategory" class="string control-label">Category</label>
 <div class="controls">
 <?php // wp_dropdown_categories(); ?>
 <?php $select_cats = wp_dropdown_categories( array( 'echo' => 0, 'taxonomy' => 'cat_story', 'hide_empty' => 0 ) );
   $select_cats = str_replace( "name='cat' id=", "name='cat[]' multiple='multiple' id=", $select_cats );
   echo $select_cats; ?>
 </div>
 </div>
 <br/>
 
 <!-- post tags -->
 <div class="control-group string postTags">
 <label for="postTags" class="string control-label">Tags (comma separated)</label>
 <div class="controls">
 <input type="text" name="postTags" id="postTags" class="string span6">
 </div>
 </div>
 
 <br/>
 <fieldset class="submit">
 <input type="submit" value="Post Review" tabindex="40" id="submit" name="submit" class="btn-sm" />
 </fieldset>

 <input type="hidden" name="action" value="frontnewPost" />
   <?php wp_nonce_field( 'new-post' ); ?>
   
 </form>
 </div> 
 <!-- END WPCF7 -->
 
     <!-- END OF FORM -->
     </div>





<?php 

if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_post") {
 
    // Do some minor form validation to make sure there is content
    if (isset ($_POST['title'])) {
        $title =  $_POST['title'];
    } else {
        echo 'Please enter the wine name';
    }
  if (isset ($_POST['wname'])) {
       $_POST['wname'];
    } else {
        echo 'Please enter the wine name';
    }
  
   
    if (isset ($_POST['description'])) {
        $description = $_POST['description'];
    } else {
        echo 'Please enter some notes';
    }
  
    $post_cat = $_POST['cat'];
    $tags = $_POST['post_tags'];
 
    // ADD THE FORM INPUT TO $new_post ARRAY
    $new_post = array(
    'post_title'    =>   $title,
    'post_content'  =>   $description,
    'post_category' =>   $post_cat,//array($_POST['cat']),  // Usable for custom taxonomies too
    'tags_input'    =>   array($tags),
    'post_status'   =>   'pending',           // Choose: publish, preview, future, draft, etc.
    'post_type' =>   'story'  //'post',page' or use a custom post type if you want to
    );
 
  
 
  update_post_meta($pid, 'writer_name', $_POST['wname']);
        

  
    //SAVE THE POST
    $pid = wp_insert_post($new_post);
 
             //SET OUR TAGS UP PROPERLY
    wp_set_post_tags($pid, $_POST['post_tags']);
    
  //SET UP CATAGORY
  wp_set_post_categories($pid, $_POST['cat']);  
 
    //REDIRECT TO THE NEW POST ON SAVE
    $link = get_permalink( $pid );
    wp_redirect( $link );
 
} // END THE IF STATEMENT THAT STARTED THE WHOLE FORM
 
//POST THE POST YO
do_action('wp_insert_post', 'wp_insert_post');








?>


<!-- WINE RATING FORM -->

 <div id="container">
            <div id="content" role="main">
 
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
 
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if ( is_front_page() ) { ?>
                        <h2 class="entry-title"><?php the_title(); ?></h2>
                    <?php } else { ?>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php } ?>
 
                    <div class="form-content">
                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                        <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-## -->
 
                <?php comments_template( '', true ); ?>
 
<?php endwhile; // end of the loop. ?>
 
            </div><!-- #content -->
        </div><!-- #container -->






<div class="wpcf7">
<form id="new_post" name="new_post" method="post" action="" class="wpcf7-form" enctype="multipart/form-data">
    <!-- post name -->
    <fieldset name="name">
        <label for="title">Story Title:</label>
        <input type="text" id="title" value="" tabindex="5" name="title" />
    </fieldset>
   <div class="form-group row">
              <label for="wname" class="col-sm-3 col-form-label">Writer Name</label>
              <div class="col-sm-9"><input name="wname" id="wname" class="form-control" type="text" value="<?php echo $_POST['wname'];?>"></div>
            </div>
    <!-- post Category -->
    <fieldset class="category">
        <label for="cat">Type:</label>
      <section>
        <option for="catagory"></option>
      </section>
        <?php $select_cats = wp_dropdown_categories( array( 'echo' => 0, 'taxonomy' => 'cat_story', 'hide_empty' => 0 ) );
$select_cats = str_replace( "name='cat' id=", "name='cat[]' multiple='multiple' id=", $select_cats );
echo $select_cats; 

?>























<!-- <div id="container">
            <div id="content" role="main">
 
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
 
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if ( is_front_page() ) { ?>
                        <h2 class="entry-title"><?php the_title(); ?></h2>
                    <?php } else { ?>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php } ?>
 
                    <div class="form-content">
                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                        <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
                    </div>
                </div>
                <?php comments_template( '', true ); ?>
 
<?php endwhile; // end of the loop. ?>
 
            </div>
        </div>--> 






























<div id="postbox">
    
    <form id="new_post" name="new_post" method="post" action="">
    
        <p><label for="title">Title</label><br />
        <input type="text" id="title" value="" tabindex="1" size="20" name="title" /></p>
        
        <p><label for="description">Description</label><br />
        <textarea id="description" tabindex="3" name="description" cols="50" rows="6"></textarea>
        </p>
        
        <p><?php wp_dropdown_categories( 'show_option_none=Category&tab_index=4&taxonomy=cat_story' ); ?></p>
        
        <p><label for="post_tags">Tags</label>
        <input type="text" value="" tabindex="5" size="16" name="post_tags" id="post_tags" /></p>
        
        <p><input type="submit" value="Publish" tabindex="6" id="submit" name="submit" /></p>
        
        <input type="hidden" name="post_type" id="post_type" value="story" />
        <input type="hidden" name="action" value="post" />

        <?php wp_nonce_field( 'new-post' ); ?>
    
    </form>

</div>





  
<?php get_footer(); ?>