<?php
/**
 * Template Name: Story Form Page Template
 *
 */

get_header(); ?>

<div class="row">
<div class="col-lg-9">
<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }
    ?>
</div>

<?php


if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['post_type_custom'] == 'story' ) {
    
    if( !$_POST['cname']){
    $error .=' <strong>ERROR</strong> Please Enter Customer Name.<br/>';
    
    }
/*  if( !$_POST['designation']){
    $error .=' <strong>ERROR</strong> Please Enter Customer Designation.<br/>';
    
    }*/
    if( !$_POST['cname']){
    $error .=' <strong>ERROR</strong> Please Enter Company Name.<br/>';
    
    }
    

    
        if(isset($_FILES['cphoto']['type']) && $_FILES['cphoto']['type']!='')
    {
        if ($_FILES['cphoto']['type'] != "image/gif" && $_FILES['cphoto']['type'] != "image/jpg" && $_FILES['cphoto']['type'] != "image/jpeg" && $_FILES['cphoto']['type'] != "image/png")
       {
           $error .='<strong>ERROR</strong> Customer photo should be in .gif, .png, .jpg or .jpeg format.<br/>';
       }
       if ($_FILES['cphoto']['size'] > 2097152)
       {
            $error .='<strong>ERROR</strong> Customer photo should be less than 2 mb.<br/>';
       }
    }
    /*else
    {
        $error .='<strong>ERROR</strong> Customer photo is required.<br/>';
    }*/
    
       



    if ( !$error )
    {
        if (isset ($_POST['comname'])) { $title = $_POST['comname']; }
        if (isset ($_POST['testi'])) { $description = $_POST['testi']; }
        
        
        $post = array(
        'post_title'    => $title,
        'post_content'  => $description,
        //'post_category'   => $cat, // Usable for custom taxonomies too
        //'tags_input'  => $tags,
        'post_status'   => 'pending', 
        'post_type' => 'story' // Set the post type based on the IF is post_type X
        );
        $post_id = wp_insert_post($post); // Pass the value of $post to WordPress the insert function
        
        update_post_meta($post_id, 'writer_name', $_POST['cname']);
        update_post_meta($post_id, 'catagory', $_POST['bio']);
        update_post_meta($post_id, 'own_str', $_POST['designation']);
       
       add_custom_image_Files($post_id, $_FILES['banner'], true);
        $cphoto = add_custom_image_Files($post_id, $_FILES['cphoto'], false);
        
        update_post_meta($post_id, 'writer_pic', $cphoto);
     
        
      

    }

}

?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      
       
      
        <h2 class="heading_title"><?php the_title(); ?></h2>

        <div class="post" id="post-<?php the_ID(); ?>">

            <div class="entry">

                <?php the_content(); ?>
                <?php if ( $error ) : ?>
        <p class="error"> <?php echo $error; ?> </p>
        <!-- .error -->
        <?php endif; ?>
        
              <form id="new_post" class="testimonials_form" enctype="multipart/form-data" name="new_post" method="post" action="">
              <div class="form-group row">
              <label for="cname" class="col-sm-3 col-form-label">Customer Name</label>
              <div class="col-sm-9"><input name="cname" id="cname" class="form-control" type="text" value="<?php if($error) echo $_POST['cname'];?>"></div>
            </div>
              
              <div class="form-group row">
              <label for="cphoto" class="col-sm-3 col-form-label">Customer Photo</label>
               <div class="col-sm-9"><input name="cphoto" id="cphoto" class="form-control" type="file"> <a href="#"  class="tooltip-right message_arrow" data-tooltip="Max upload file size 2 Mb"> <img src="<?php bloginfo('template_directory');?>/assets/images/message_bg.png" width="14" height="14" alt=""> </a> </div>
             </div>
               
                <div class="form-group row">
                <label for="designation" class="col-sm-3 col-form-label">Customer Designation</label>
                <div class="col-sm-9"><input name="designation" id="designation" class="form-control" type="text" value="<?php if($error) echo $_POST['designation'];?>"></div>
              </div>

               <div class="form-group row">
                <label for="comname" class="col-sm-3 col-form-label">Company Name</label>
                <div class="col-sm-9"><input name="comname" id="comname" class="form-control" type="text" value="<?php if($error) echo $_POST['comname'];?>"></div>
              </div>

                
                <div class="form-group row">
               
                <div class="col-sm-9"><input type="submit" class="btn btn-primary" value="Submit" tabindex="6" id="" name="" /></div></div>

                <input type="hidden" name="post_type_custom" id="post_type_custom" value="story" />
                
                <input type="hidden" name="action" value="post" />
                
                <?php wp_nonce_field( 'new-post' ); ?>
                
               </form>
              

            </div>

            <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

        </div>
        
        <?php endwhile; endif; ?>
</div>
  
 
  

  
  

<?php get_sidebar(); ?>
<div class="clear"></div>
</div>
<?php get_footer(); ?>