<?php
/**
 * Template Name: Testimonial Form Page Template
 *
 */

get_header(); ?>

<div class="container">
<div class="">


<?php
$securimage = new Securimage(); 
    
    $args = array(
    'blog_id'      => $GLOBALS['blog_id'],
    'role'         => 'subscriber',
    'meta_key'     => 'last_name',
    'meta_value'   => '',
    'meta_compare' => '',
    'meta_query'   => array(),
    'include'      => array(),
    'exclude'      => array(),
    'orderby'      => 'meta_value',
    'order'        => 'ASC',
    'offset'       => '',
    'search'       => '',
    'number'       => '',
    'count_total'  => false,
    'fields'       => 'all',
    'who'          => ''
 );
$path = get_theme_root();
$target_path = substr($path, 0,strpos($path,'theme'));
$target_path = $target_path."uploads/testimonials-images/";

if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['post_type_custom'] == 'client_spotlight' ) {
  
  if( !$_POST['comname']){
    $error .=' <strong>ERROR</strong> Please Enter Story Title.<br/>';
    
    }
    
    if( !$_POST['bio']){
    $error .=' <strong>ERROR</strong> Please Enter Story Catagory.<br/>';
    
    }
    
    if( !$_POST['testi']){
    $error .=' <strong>ERROR</strong> Please Enter Your Story.<br/>';
    
    }
  
    if( !$_POST['cname']){
    $error .=' <strong>ERROR</strong> Please Enter Your Name.<br/>';
    
    }
  if( !$_POST['designation']){
    $error .=' <strong>ERROR</strong> Please Enter Phone Number.<br/>';
    
    }
  if( !$_POST['email']){
    $error .=' <strong>ERROR</strong> Please Enter Your email.<br/>';
    
    }
  
   
    
    if(isset($_FILES['banner']['type']) && $_FILES['banner']['type']!='')
    {
        
        list($width, $height, $type, $attr) = getimagesize($_FILES["banner"]['tmp_name']);
       if (($width != 994) &&  ($height != 356))
       {
           $error .='<strong>ERROR</strong> Banner image resolution should be 994 x 356 px.<br/>';
       }
       
       
        if ($_FILES['banner']['type'] != "image/gif" && $_FILES['banner']['type'] != "image/jpg" && $_FILES['banner']['type'] != "image/jpeg" && $_FILES['banner']['type'] != "image/png")
       {
           $error .='<strong>ERROR</strong> Banner Image should be in .gif, .png, .jpg or .jpeg format.<br/>';
       }
       if ($_FILES['banner']['size'] > 4194304)
       {
            $error .='<strong>ERROR</strong> Banner Image should be less than 4 mb.<br/>';
       }
    }
    else
    {
        $error .='<strong>ERROR</strong> Banner Image is required.<br/>';
    }
    
        if(isset($_FILES['cphoto']['type']) && $_FILES['cphoto']['type']!='')
    {
        if ($_FILES['cphoto']['type'] != "image/gif" && $_FILES['cphoto']['type'] != "image/jpg" && $_FILES['cphoto']['type'] != "image/jpeg" && $_FILES['cphoto']['type'] != "image/png")
       {
           $error .='<strong>ERROR</strong> Your photo should be in .gif, .png, .jpg or .jpeg format.<br/>';
       }
       if ($_FILES['cphoto']['size'] > 2097152)
       {
            $error .='<strong>ERROR</strong> Your photo should be less than 2 mb.<br/>';
       }
    }
   else
    {
        $error .='<strong>ERROR</strong> Your Photo is required.<br/>';
    }
    
  /*      if(isset($_FILES['clogo']['type']) && $_FILES['clogo']['type']!='')
    {
        if ($_FILES['clogo']['type'] != "image/gif" && $_FILES['clogo']['type'] != "image/jpg" && $_FILES['clogo']['type'] != "image/jpeg" && $_FILES['clogo']['type'] != "image/png")
       {
           $error .='<strong>ERROR</strong> Company logo should be in .gif, .png, .jpg or .jpeg format.<br/>';
       }
       if ($_FILES['clogo']['size'] > 2097152)
       {
            $error .='<strong>ERROR</strong> Company logo should be less than 2 mb.<br/>';
       }
    }
    else
    {
        $error .='<strong>ERROR</strong> Company logo is required.<br/>';
    }
    */
    
  

    
    if ($securimage->check($_POST['captcha_code']) == false) {
          // the code was incorrect
          // you should handle the error so that the form processor doesn't continue
        
          // or you can use the following code if there is no validation or you do not know how
          $error .= "<strong>ERROR</strong> The security code entered was incorrect.<br />";
    //    $error .= "<strong>ERROR</strong> Please go <a href='javascript:history.go(-1)'>back</a> and try again.";
          //exit;
}

    if ( !$error )
    {
        if (isset ($_POST['comname'])) { $title = $_POST['comname']; }
        if (isset ($_POST['testi'])) { $description = $_POST['testi']; }
    
      
      
        $post = array(
        'post_title'    => $title,
        'post_content'  => $description,
        'post_category'   => $cat, // Usable for custom taxonomies too
        'tags_input'  => $tags,
        'post_status'   => 'pending', 
        'post_type' => 'story' // Set the post type based on the IF is post_type X
        );
        $post_id = wp_insert_post($post); // Pass the value of $post to WordPress the insert function
      
      
        update_post_meta($post_id, 'writer_name', $_POST['cname']);
        update_post_meta($post_id, 'catagory', $_POST['bio']);
        update_post_meta($post_id, 'writer_phone', $_POST['designation']);
       update_post_meta($post_id, 'writer_email', $_POST['email']);
   //     update_post_meta($post_id, 'website_url', $_POST['url']);
        
        
        add_custom_image_Files($post_id, $_FILES['banner'], true);
    //   $clogo = add_custom_image_Files($post_id, $_FILES['clogo'], false);
        $cphoto = add_custom_image_Files($post_id, $_FILES['cphoto'], false);
        
       update_post_meta($post_id, 'writer_pic', $cphoto);
      
      
      
     //   update_post_meta($post_id, 'company_logo', $clogo);
        
    //  header("Location: " . esc_url( get_permalink( 116 ) ));
   
 $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . 'thank-you';
      
      $redirect_url = "<script type='text/javascript'>window.location='".$home_url."';</script>";
     
      echo($redirect_url);
      die();

      
   
      
 
    }

}

?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php if(has_post_thumbnail()!=''){ ?>
        <div class="page_banner"><?php the_post_thumbnail(); ?></div>   
        <?php } ?>
        <h2 class="title"><?php the_title(); ?></h2>
       

        <div class="post" id="post-<?php the_ID(); ?>">

            <div class="entry">

                <?php the_content(); ?>
                <?php if ( $error ) : ?>
        <p class="error"> <?php echo $error; ?> </p>
        <!-- .error -->
        <?php endif; ?>
        
              <form id="new_post" class="testimonials_form" enctype="multipart/form-data" name="new_post" method="post" action="">
             
                 <div class="form-group">
                <input name="comname" id="comname" type="text" placeholder="Story Title *" value="<?php if($error) echo $_POST['comname'];?>">
              </div>
                 <div class="form-group">
              <input name="bio" id="bio" cols="" rows="" placeholder="Story Catagory *" value="<?php if($error) echo $_POST['bio'] ?>">
              </div>
                  <div class="form-group">
             <textarea name="testi" id="testi" cols="40" rows="10" placeholder="Your Story *"><?php if($error) echo $_POST['testi'] ?></textarea>
               </div>
                <div class="form-group">
                <label for="banner" class="form-label">Story Banner Image (should be 994 x 356 px.)</label>
                <input name="banner" id="banner" type="file" placeholder="Story Banner Image (should be 994 x 356 px.)"> <a href="#"  class="tooltip-right message_arrow" data-tooltip="Max upload file size 3 Mb & Banner image size should be 994 x 356 px."> <img src="<?php bloginfo('template_directory');?>/assets/images/message_bg.png" width="14" height="14" alt=""> </a>
                </div>   
               <!-- <div class="form-group row">
              <label for="cname" class="col-sm-3 col-form-label">Customer Name</label>
              <div class="col-sm-9"><input name="cname" id="cname" class="form-control" type="text" value="<?php if($error) echo $_POST['cname'];?>"></div>
            </div> -->
                
            <div class="form-group">
             <input name="cname" id="cname" type="text" placeholder="Name *" value="<?php if($error) echo $_POST['cname'];?>">
            </div> 
                
            <div class="form-group">
          <input name="designation" id="designation" type="tel" placeholder="Phone Number *" value="<?php if($error) echo $_POST['designation'];?>">
            </div> 
                
                 <div class="form-group">
       <input name="email" id="email" type="email" placeholder="Email Address *" value="<?php if($error) echo $_POST['email'];?>">
              </div>
                
                 <div class="form-group">
              <label for="cphoto" class="form-label">Your Photo</label>
              <input name="cphoto" id="cphoto" placeholder="Your Photo" type="file"> <a href="#"  class="tooltip-right message_arrow" data-tooltip="Max upload file size 2 Mb"> <img src="<?php bloginfo('template_directory');?>/assets/images/message_bg.png" width="14" height="14" alt=""> </a>
             </div>
                
                
             <!--<div class="form-group row">
              <label for="catagory" class="col-sm-3 col-form-label">Catagory</label>
              <div class="col-sm-9"> <?php $select_cats = wp_dropdown_categories( array( 'echo' => 0, 'taxonomy' => 'cat_story', 'hide_empty' => 0 ) );
$select_cats = str_replace( "name='cat' id=", "name='cat[]' multiple='multiple' id=", $select_cats );
echo $select_cats; ?>
                <input name="catagory" id="catagory" class="form-control" type="text" value="<?php echo $select_cats; ?>"></div>
            </div> -->

                
           <!--      <div class="form-group row">
                <label for="url" class="col-sm-3 col-form-label">Website Url</label>
                <div class="col-sm-9"><input name="url" id="url" class="form-control" type="text" value="<?php if($error) echo $_POST['url'];?>"></div>
              </div> -->
               
              <!--  <div class="form-group row">
              <label for="clogo" class="col-sm-3 col-form-label">Company Logo</label>
               <div class="col-sm-9"><input name="clogo" id="clogo" class="form-control" type="file"> <a href="#"  class="tooltip-right message_arrow" data-tooltip="Max upload file size 2 Mb"> <img src="<?php bloginfo('template_directory');?>/assets/images/message_bg.png" width="14" height="14" alt=""> </a></div>
                </div> -->

                              
        
                
               
            
                
               <!-- <p> <label for="">Input this code</label><input name="" type="text"></p> -->
                <div class="form-group">
                <label for="lead_title" class="form-label">Input This Code</label>
               <div class="captcha"><img id="captcha" src="<?php echo get_bloginfo('template_directory');?>/securimage/securimage_show.php" alt="CAPTCHA Image" />
                    <a href="#" onclick="document.getElementById('captcha').src = '<?php echo get_bloginfo('template_directory');?>/securimage/securimage_show.php?' + Math.random(); return false"></a>
                  
                    <input type="text" name="captcha_code" size="10" maxlength="6" placeholder="Input This Code"/>
                    
                    

                </div>
              </div>
               <div class="button">
              <span class="button-effect"></span>
               <input type="submit" value="SUBMIT" tabindex="6" id="" name="" />
                </div>

                <input type="hidden" name="post_type_custom" id="post_type_custom" value="client_spotlight" />
                
                <input type="hidden" name="action" value="post" />
                
                <?php wp_nonce_field( 'new-post' ); ?>

                
               
               </form>
               

            </div>

            <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

        </div>
        
        <?php endwhile; endif; ?>
</div>

<?php //get_sidebar(); ?>
<div class="clear"></div>
</div>

<?php get_footer(); ?>