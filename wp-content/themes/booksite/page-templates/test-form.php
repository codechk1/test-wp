<?php
/**
*Template Name:TEST form
*
*/
 get_header(); ?>


<?php 

 $postTitleError = '';
 $postContentError = '';
 
 if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "frontnewPost" ) {
 
 if ( trim( $_POST['postTitle'] ) === '' ) {
 $postTitleError = 'Please enter a title.';
 $hasError = true;
 }
 if ( trim( $_POST['postContent'] ) === '' ) {
 $postContentError = 'Please enter the post content.';
 $hasError = true;
 }
 
 if (isset ($_POST['postTitle'])) {
         $postTitle =  $_POST['postTitle'];
     }
 
     if (isset ($_POST['postContent'])) {
         $postContent = $_POST['postContent'];
     } 
 
 if ( isset($_POST['postTitle']) && isset($_POST['postContent']) && ( $hasError==false )) {
 $postTags = trim($_POST['postTags']);
 global $wpdb;
 
     	$new_post = array(
     'post_title'    =>   $postTitle,
     'post_content'  =>   $postContent,
     'post_category' =>   $_POST['cat'],//array($_POST['cat']), // if specific category, then set it's id like: array(4),  
     'tags_input'    =>   array($postTags),
     'post_status'   =>   'publish',         
     'post_type' =>   'story'  
     );
 
     	$post_id = wp_insert_post($new_post);
  
     	wp_set_post_tags($post_id, $_POST['postTags']);
 
     	$link = get_permalink( $post_id );
 echo "<meta http-equiv='refresh' content='0;url=$link' />"; exit;
 }
 
  
   
 
 }


?>
<div class="container">

    <!-- FORM -->
 <div class="wpcf7">
 <form id="new_post" name="new_post" method="post" action="" class="wpcf7-form form-horizontal" enctype="multipart/form-data" role="form">
 
 <div class="control-group string postTitle">
 <label for="postTitle" class="string control-label">Title</label>
 <div class="controls">
 <input type="text" name="postTitle" id="postTitle" class="string span6" value="<?php if ( isset( $_POST['postTitle'] ) ) echo $_POST['postTitle']; ?>">
 </div>
 </div>
 <br/>
 <!-- post Content -->
 <div class="control-group string postContent">
 <label for="postContent" class="string control-label">Contents</label>
 <div class="controls">
 <textarea id="postContent" tabindex="15" name="postContent" cols="80" rows="10"><?php if ( isset( $_POST['postContent'] ) ) { if ( function_exists( 'stripslashes' ) ) { echo stripslashes( $_POST['postContent'] );} else { echo $_POST['postContent'];}} ?></textarea>
 </div>
 </div>
 <br/>
 
 <!-- post Category  -->
 <div class="control-group string postCategory">
 <label for="postCategory" class="string control-label">Category</label>
 <div class="controls">
 <?php // wp_dropdown_categories(); ?>
 <?php $select_cats = wp_dropdown_categories( array( 'echo' => 0, 'taxonomy' => 'cat_story', 'hide_empty' => 0 ) );
   $select_cats = str_replace( "name='cat' id=", "name='cat[]' multiple='multiple' id=", $select_cats );
   echo $select_cats; ?>
 </div>
 </div>
 <br/>
 
 <!-- post tags -->
 <div class="control-group string postTags">
 <label for="postTags" class="string control-label">Tags (comma separated)</label>
 <div class="controls">
 <input type="text" name="postTags" id="postTags" class="string span6">
 </div>
 </div>
 
 <br/>
 <fieldset class="submit">
 <input type="submit" value="Post Review" tabindex="40" id="submit" name="submit" class="btn-sm" />
 </fieldset>

 <input type="hidden" name="action" value="frontnewPost" />
   <?php wp_nonce_field( 'new-post' ); ?>
   
 </form>
 </div> 
 <!-- END WPCF7 -->
 
     <!-- END OF FORM -->
     </div>


</div>


<?php 

if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_post") {
 
    // Do some minor form validation to make sure there is content
    if (isset ($_POST['title'])) {
        $title =  $_POST['title'];
    } else {
        echo 'Please enter the wine name';
    }
  if (isset ($_POST['wname'])) {
       $_POST['wname'];
    } else {
        echo 'Please enter the wine name';
    }
  
   
    if (isset ($_POST['description'])) {
        $description = $_POST['description'];
    } else {
        echo 'Please enter some notes';
    }
  
    $post_cat = $_POST['cat'];
    $tags = $_POST['post_tags'];
 
    // ADD THE FORM INPUT TO $new_post ARRAY
    $new_post = array(
    'post_title'    =>   $title,
    'post_content'  =>   $description,
    'post_category' =>   $post_cat,//array($_POST['cat']),  // Usable for custom taxonomies too
    'tags_input'    =>   array($tags),
    'post_status'   =>   'pending',           // Choose: publish, preview, future, draft, etc.
    'post_type' =>   'story'  //'post',page' or use a custom post type if you want to
    );
 
  
 
  update_post_meta($pid, 'writer_name', $_POST['wname']);
        

  
    //SAVE THE POST
    $pid = wp_insert_post($new_post);
 
             //SET OUR TAGS UP PROPERLY
    wp_set_post_tags($pid, $_POST['post_tags']);
    
  //SET UP CATAGORY
  wp_set_post_categories($pid, $_POST['cat']);  
 
    //REDIRECT TO THE NEW POST ON SAVE
    $link = get_permalink( $pid );
    wp_redirect( $link );
 
} // END THE IF STATEMENT THAT STARTED THE WHOLE FORM
 
//POST THE POST YO
do_action('wp_insert_post', 'wp_insert_post');








?>


<!-- WINE RATING FORM -->

 <div id="container">
            <div id="content" role="main">
 
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
 
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if ( is_front_page() ) { ?>
                        <h2 class="entry-title"><?php the_title(); ?></h2>
                    <?php } else { ?>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php } ?>
 
                    <div class="form-content">
                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                        <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-## -->
 
                <?php comments_template( '', true ); ?>
 
<?php endwhile; // end of the loop. ?>
 
            </div><!-- #content -->
        </div><!-- #container -->






  
<?php get_footer(); ?>