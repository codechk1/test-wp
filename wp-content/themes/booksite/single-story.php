<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="container">
  <div class="row">
		<div <?php post_class("col-lg-8") ?> id="post-<?php the_ID(); ?>">
			
			<h2><?php the_title(); ?></h2>
      <div class="single-image"><?php the_post_thumbnail(); ?></div>
			<?php //include (TEMPLATEPATH . '/inc/meta.php' ); ?>
      <div class="meta">
                <div class="time"><span><i class="far fa-calendar-alt"></i> <?php the_time('F d, Y') ?></span><span><i class="fas fa-user"></i> <?php the_field('writer_name');?></span><span><i class="fas fa-tags"></i> <?php the_field('catagory');?></span></div>
                <?php //comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
              </div>
			<div class="entry">
        <p><?php the_content(); ?></p>
        <div class="writer-info story-home">
           <h2>Writer Personal Details</h2>
        <div class="row">
          <div class="writer_pic col-md-4"><img src="<?php echo the_field('writer_pic'); ?>" alt="" width="130"/></div>
        <div class="col-md-8">
       
        <p>WRITER NAME : <?php the_field('writer_name');?></p>
        
        <p>WRITER NUMBER: <?php the_field('writer_phone');?></p>
        <p>WRITER EMAIL ADDRESS: <?php the_field('writer_email');?></p>
          </div>
          </div>
          </div>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
				
				<?php the_tags( 'Tags: ', ', ', ''); ?>

			</div>
			<?php //comments_template(); ?>
      
			<?php edit_post_link('Edit this entry','','.'); ?>
    </div>
	
    <div class="col-lg-4"><div id="secondary" class="secondary"><?php dynamic_sidebar( 'sidebar-2' ); ?></div></div>
  
  </div>

	

	<?php endwhile; endif; ?>
	

</div>
<?php get_footer(); ?>