<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="container">
  <div class="row">
		<div <?php post_class("col-lg-8") ?> id="post-<?php the_ID(); ?>">
			
			<h2><?php the_title(); ?></h2>
      <div class="single-image"><?php the_post_thumbnail(); ?></div>
			<?php //include (TEMPLATEPATH . '/inc/meta.php' ); ?>
      <div class="meta">
               <div class="time wow fadeInUp"><span><i class="far fa-calendar-alt"></i> <?php the_time('F d, Y') ?></span><span><i class="fas fa-user"></i> <?php the_author() ?></span><span><i class="fas fa-tags"></i> <?php the_category(', ') ?></span></div>
                <?php //comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
              </div>
			<div class="entry">
				
				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
				
				<?php the_tags( 'Tags: ', ', ', ''); ?>

			</div>
			<?php //comments_template(); ?>
      
			<?php edit_post_link('Edit this entry','','.'); ?>
    </div>
	
    <div class="col-lg-4"><?php get_sidebar(); ?></div>	
  
  </div>

	

	<?php endwhile; endif; ?>
	

</div>
<?php get_footer(); ?>