<?php get_header();

$queried_object = get_queried_object(); ?>

<div class="container">
  <div class="row">
		  <div class="col-lg-8">
         <?php query_posts( array('posts_per_page' => '-1', 'post_type' => 'story', 'orderby' =>'menu_order', 'order' => 'ASC', 'cat_story' => $queried_object->slug));
        
       
        
			  if (have_posts()) : while (have_posts()) : the_post(); ?>
			
				<div <?php post_class() ?>>
			
					<h2 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>
          <div class="single-image"><?php the_post_thumbnail(); ?></div>
          <?php //description(); ?>
         
          <div class="meta">
                <div class="time"><span><i class="far fa-calendar-alt"></i> <?php the_time('F d, Y') ?></span><span><i class="fas fa-user"></i> <?php the_field('writer_name');?></span><span><i class="fas fa-tags"></i> <?php the_field('catagory');?></span></div>
                <?php //comments_popup_link('No Comments', '1 Comment', '% Comments', 'comments-link', ''); ?>
              </div>
					<div class="entry">
						<?php the_excerpt(); ?>
					</div>
           <div class="blog-button wow fadeInUp"><a class="read" href="<?php the_permalink(); ?>">Read More</a></div>
   </div>
			<?php endwhile; ?>
        
         <ul class="pagination">
            <li><?php echo get_next_posts_link( '<i class="fas fa-arrow-right"></i>', $the_query->max_num_pages ); ?></li>
            <li><?php echo get_previous_posts_link( '<i class="fas fa-arrow-left"></i>' ); ?></li>
        </ul>
        
        </div>
    <div class="col-lg-4"><div id="secondary" class="secondary"><?php dynamic_sidebar( 'sidebar-2' ); ?></div></div>
			
   </div>

	<?php else : ?>

		<h2>Nothing found</h2>

	<?php endif; ?>

</div>
<?php get_footer(); ?>